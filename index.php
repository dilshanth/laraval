<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>
			themeforest
		</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="css/theme.css" />
	</head>
	<body>
		<div class="jumbotron jumbotron-fluid header">
			<div class="container">
				<span><a class="adr" href="#">1-800-1234-567,1-800-8655-098</a></span>
				<span><a class="adr" href="#">1-800-1234-567,1-800-8655-098</a></span>
				<span><a class="adr" href="#">mail@demolink.org</a></span>
			</div>
		</div>
		<div class="jumbotron jumbotron-fluid panel fixed-top">
			<div class="container">
				<nav class="navbar navbar-expand-sm">
					<!-- Brand/logo -->
					<a class="navbar-brand" href="#">
						<img src="https://s3.envato.com/files/250611621/thumbnail.png" height="70px" width="100px" />
					</a>
  
					<!-- Links -->
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link home" href="#">HOME</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">SERVICES</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">BLOG</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">PAGES</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">CONTACT</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<div class="parallax">
			<div class="row">
				<div class="col-md-6 col-xl-6 col-xxl-5 col-mxl-4 inset-modern-left">
					<h1 class="para-content">
						We Can Improve The <span class="clr">Web Image</span> of 
						<span class="text-italic">
							Your
						</span> Company
					</h1>
					<div class="btn-group-vertical">
						<div><a href="#" class="btn btn-one btn-block">FREE SITE ANALYSIS</a></div>
						<div><a href="#" class="btn btn-second btn-block">MORE DETAILS</a></div>
					</div>
				</div>
				<div class="col-md-6 col-lg-5 col-xl-4 col-xxl-4 col-mxl-3 inset-modern-right csn">
					<div class="block-book-consultation">
						<h4 class="h-book">Book a Consultation</h4>
						<p class="p-book">Get free advice from our SEO and SMM experts just by filling out this form.</p>
						<form>
							<div class="row row-center">
								<div class="col-md-12">
									<div><label>First name</label></div><input type="text" class="ipt" />
								</div>	
								<div class="col-md-12">
									<div><label>Phone</label></div><input type="tel" class="ipt" />
								</div>	
								<div class="col-md-12">
									<div><label>E-mail</label></div><input type="email" class="ipt" />
								</div>
							</div>
							<div class="form-button">
								<button type="button" class="btn book-btn">FREE CONSOLATION</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<section class="section section-lg bg-white text-center">
			<div class="container ctn">
				<h2 style="width:1120px">Our Advantages</h2>
				<p class="block-lg">
					Working with us you can lift the web reputation of your company higher than ever.
					We can offer you all advantages of modern SEO and e-marketing services.
				</p>
				<div class="row">
					<div class="col-md-4">
						 <h3>Welcome <i class="fa fa-shield"></i></h3>
					</div>
					<div class="col-md-4"></div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</section>
	</body>
</html>